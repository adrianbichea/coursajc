#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "dungeon.h"
#include "random.h"

#define clear_screen() printf("\033[2J");
#define hide_cursor() printf("\e[?25l")
#define show_cursor() printf("\e[?25h")
#define move_cursor(x, y) printf("\033[%d;%df", (x), (y))

#define DUNGEON_SIZE 50 
#define MAX_FLOOR_CELLS 1000
#define DUNGEON_RESOLUTION_X 20
#define DUNGEON_RESOLUTION_Y 40

#define red_color() printf("\033[0;31m");
#define cancel_color() printf("\033[0m");

char dungeon[DUNGEON_SIZE + 1][DUNGEON_SIZE + 1];
int count_floor_cells = 0;

int player_position_x = 0;
int player_position_y = 0;

int turn_cell_to_floor(int x, int y)
{
	if (dungeon[x][y] == 0)//if the cell is wall then it's ok
	{
		dungeon[x][y] = 1;
		return 1;
	}
	return 0;
}

int get_direction_to_move(int x, int y)
{
	int dirs[4];
	int counter_dirs = 0;
	int dir_found = 0;
	for (int i = 0; i < 4; i++)
	{
		dirs[i] = 0;
	}
	if (x - 1 >= 1 && dungeon[x - 1][y] == 0)
	{
		dir_found = 1;
		dirs[counter_dirs++] = 1;//N
	}
	if (x + 1 <= DUNGEON_SIZE - 1 && dungeon[x + 1][y] == 0)
	{
		dir_found = 1;
		dirs[counter_dirs++] = 2;//S
	}
	if (y - 1 >= 1 && dungeon[x][y - 1] == 0)
	{
		dir_found = 1;
		dirs[counter_dirs++] = 3;//W
	}
	if (y + 1 <= DUNGEON_SIZE - 1 && dungeon[x][y + 1] == 0)
	{
		dir_found = 1;
		dirs[counter_dirs++] = 4;//E
	}
	if (dir_found)
	{
		int p =	counter_dirs == 1 ? 0 : rand(0, counter_dirs - 1);
		return dirs[p];
	}
	return 0;
}

void create_dungeon()
{
	printf("Starting ...\n");
	//al dungeon has walls
	for (int i = 0; i <= DUNGEON_SIZE; i++)
	{
		for (int j = 0; j <= DUNGEON_SIZE; j++)
		{
			dungeon[i][j] = 0;
		}
	}
	//pick a random start point
	int x = rand(1, DUNGEON_SIZE - 1);
	int y = rand(1, DUNGEON_SIZE - 1);
	//setting up the player starting position
	player_position_x = x;
	player_position_y = y;
	//this tile map is floor now
	dungeon[x][y] = 1;
	count_floor_cells = 1;
	int watch_dog = 100;
	clear_screen();
	while (count_floor_cells < MAX_FLOOR_CELLS)
	{
		float perc = 100.f * (float)count_floor_cells / (float)MAX_FLOOR_CELLS;
		printf("\rGenerated: %.02f %", perc);
		int dir = get_direction_to_move(x, y);//direction to move, 1-N 2-S 3-W 4-E
		int a = 1;//preparing direction
		if (dir == 1 || dir == 3)
		{
			a = -1;
		}
		if (dir == 1 || dir == 2)//if going to N or S (x axis)
		{
			if (x + a < DUNGEON_SIZE && x + a > 0)//check if i'm in the map
			{
				if (turn_cell_to_floor(x + a, y))//turn cell to floor
				{
					x += a;//moving to new position
					watch_dog = 100;
					count_floor_cells++;
				}
			}
		}
		else if (dir == 3 || dir == 4)
		{
			if (y + a < DUNGEON_SIZE && y + a > 0)//cheking map position
			{
				if (turn_cell_to_floor(x, y + a))//turn cell to floor
				{
					y += a;//moving to new pos
					watch_dog = 100;
					count_floor_cells++;
				}
			}
		}
		else //dir == 0
		{
			//choose the first floor (random) with valid direction
			int ok = 0;
			int wd = 1000;//watch dog.. to not get stuck
			while (!ok)
			{
				x = rand(1, DUNGEON_SIZE - 1);
				y = rand(1, DUNGEON_SIZE - 1);
				if (dungeon[x][y] == 1 && get_direction_to_move(x, y) != 0)
				{
					ok = 1;
				}
				wd--;
				if (wd <= 0)
				{
					ok = 1;
				}
			}
			if (dungeon[x][y] == 0)
			{
				//this sucks, then i will search the first valid one
				for (int i = 1; i < DUNGEON_SIZE; i++)
				{
					int found = 0;
					for (int j = 1; j < DUNGEON_SIZE; j++)
					{
						if (dungeon[i][j] == 1 &&
							get_direction_to_move(x, y) != 0)
						{
							x = i;
							y = j;
							found = 1;
							break;
						}
					}
					if (found)
					{
						break;
					}
				}
			}
		}
	}
	//dungeon is generated
	count_floor_cells = 0;
	for (int i = 0; i < DUNGEON_SIZE; i ++)
	{
		for (int j = 0; j < DUNGEON_SIZE; j++)
		{
			if (dungeon[i][j] == 1)
			{
				count_floor_cells++;
			}
		}
	}
}

int get_start(int resolution, int pos)
{
	int mr = resolution / 2;
	if (pos < mr)
	{
		return 0;
	}
	else if (pos > DUNGEON_SIZE + 1 - mr)
	{
		return DUNGEON_SIZE + 1 - resolution;
	}
	return pos - mr;
}

int command_player(char command)
{
	if (command == 'n')
	{
		if (dungeon[player_position_x - 1][player_position_y])
		{
			player_position_x--;
			return 1;
		}
	}
	else if (command == 's')
	{
		if (dungeon[player_position_x + 1][player_position_y])
		{
			player_position_x++;
			return 1;
		}
	}
	else if (command == 'w')
	{
		if (dungeon[player_position_x][player_position_y - 1])
		{
			player_position_y--;
			return 1;
		}
	}
	else if (command == 'e')
	{
		if (dungeon[player_position_x][player_position_y + 1])
		{
			player_position_y++;
			return 1;
		}
	}

	return 0;
}

void print_pos(int x, int y, char* text)
{
	for (int i = 0; i < strlen(text); i++)
	{
		move_cursor(x, y + i);
		putchar(text[i]);
	}
}

void show_help()
{
	print_pos(1, 44, "HELP COMMANDS");
	print_pos(2, 44, "To move:");
	print_pos(3, 44, "   n - North");
	print_pos(4, 44, "   s - South");
	print_pos(5, 44, "   w - West");
	print_pos(6, 44, "   e - East");
	print_pos(7, 44, "   ?  - Help");

	print_pos(1, 60, "ENEMIES");
	print_pos(2, 60, "   @ - Wind");
	print_pos(3, 60, "   ~ - Water");
	print_pos(4, 60, "   M - Fire");
	print_pos(5, 60, "   * - Ice");

	print_pos(7, 60, "   x - Player");
}

void show_dungeon()
{
	//hide_cursor();
	//move_cursor(i, j);
	//putchar('*');
	clear_screen();
	show_help();

	int start_x = get_start(DUNGEON_RESOLUTION_X, player_position_x);
	int start_y = get_start(DUNGEON_RESOLUTION_Y, player_position_y);
	int stop_x = start_x + DUNGEON_RESOLUTION_X;
	int stop_y = start_y + DUNGEON_RESOLUTION_Y;

	int x = 1;

	for (int i = start_x; i < stop_x; i++)
	{
		int y = 1;
		for (int j = start_y; j < stop_y; j++)
		{
			move_cursor(x, y);
			if (i == player_position_x && j == player_position_y)
			{
				red_color();
				putchar('x');
				cancel_color();
			}
			else
			{
				if (dungeon[i][j])//floor
				{
					putchar(' ');
				}
				else
				{
					putchar('#');
				}
			}
			y++;
		}
		x++;
	}
	for (int i = 0; i < DUNGEON_RESOLUTION_X; i++)
	{
		move_cursor(i + 1, DUNGEON_RESOLUTION_Y + 1);
		putchar('|');
	}
	for (int i = 0; i < DUNGEON_RESOLUTION_Y; i++)
	{
		move_cursor(DUNGEON_RESOLUTION_X + 1, i + 1);
		putchar('-');
	}
	move_cursor(DUNGEON_RESOLUTION_X + 1, DUNGEON_RESOLUTION_Y + 1);
	putchar('+');
	printf("\n\nplayer pos: %d, %d\n", player_position_x, player_position_y);
	//show_cursor();
}
