#ifndef _RANDOM_H_
#define _RANDOM_H_
#include <time.h>

static unsigned int n = 1;
int firstTime = 1;

int rand(int min, int max)
{
	if (firstTime)
	{
		firstTime = 0;
		n = time(NULL);
	}
	n = (n * 125) % 2796203;
	int lim = (max - min <= 0) ? min : max - min;
	return ((n % (lim + ((max - min <= 0) ? 0 : 1))) + ((max - min <= 0) ? 1 : min));
}

#endif
