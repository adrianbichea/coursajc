#include <stdio.h>
#include "dungeon.h"

int hp = 10;

void help_commands(char r)
{
	printf("\nLife: %d", hp);
	if (r == '?')
	{
		printf("\nMove and explore. The only functionality right now :)");
		/*printf("\n");
		printf("\n");
		printf("\n");
		printf("\n");
		printf("\n");*/
	}
}

int main()
{
	create_dungeon();
	
	char r = '0';
	while (r != 'q')
	{
		show_dungeon();
		help_commands(r);
		printf("\nCommand:");
		scanf(" %c", &r);
		command_player(r);
	}
	printf("Bye!\n\n");
}
