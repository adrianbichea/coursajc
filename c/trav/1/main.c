#include <stdio.h>

void show_values(char *,int, int);
void inverse(int*, int*);

int main()
{
	int a = 10;
	int b = 20;

	show_values("Before -> ", a, b);

	inverse(&a, &b);

	show_values("After -> ", a, b);

	return 0;
}

void inverse(int *a, int *b)
{
	int t = *a;
	*a = *b;
	*b = t;
}

void show_values(char *str, int a, int b)
{
	printf("%s(%d, %d)\n", str, a, b);
}
