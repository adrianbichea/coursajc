#include <string.h>
#include <unistd.h>

#include "annuaire.h"

#define FILENAME "annuaire.txt"

int tel_exist(char *tel)
{
	FILE *f = fopen(FILENAME, "r");
	if (f == NULL)
	{
		return 0;
	}
	while (1)
	{
		s_phone_user user;
		fscanf(f, "%s %s %s", &user.nom, &user.prenom, &user.tel);
		if (feof(f))
		{
			return 0;
		}
		if (strcmp(tel, user.tel) == 0)
		{
			return 1;
		}
	}
	fclose(f);
}

int add_new()
{
	s_phone_user user;

	printf("\nNom: ");
	scanf("%s", &user.nom);
	fflush(stdin);

	printf("Prenom: ");
	scanf("%s", &user.prenom);
	fflush(stdin);

	printf("Tel: ");
	scanf("%s", &user.tel);
	fflush(stdin);

	if (tel_exist(user.tel))
	{
		printf("\n ** Phone number already into the database! **\n");
		return 2;//tel exists
	}

	FILE *f = fopen(FILENAME, "a+");
	fprintf(f, "%s %s %s ", user.nom, user.prenom, user.tel);
	fclose(f);
}

void view()
{
	FILE *f = fopen(FILENAME, "r");
	if (f == NULL)
	{
		return;
	}
	printf("\n ** View all users **\n\n");
	while (1)
	{
		s_phone_user user;
		fscanf(f, "%s %s %s", &user.nom, &user.prenom, &user.tel);
		if (feof(f))
		{
			break;
		}
		printf("%s %s - %s\n", user.nom, user.prenom, user.tel);
	}
	fclose(f);
}

int str_compare(char *source, char *find)
{
	int begin = 0;
	int end = 0;

	if (find[0] == '*')
	{
		begin = 1;
	}
	if (find[strlen(find) - 1] == '*')
	{
		end = 1;
	}

	if (strlen(find) > strlen(source))
	{
		return 1;
	}

	//printf("begin %d ' end %d\n", begin, end);

	if (begin == 0 && end == 0)
		return strcmp(source, find);

	if (begin == 1 && end == 0)
	{
		int k = strlen(find) - 1;
		for (int i = strlen(source) - 1; i >= 0; i--)
		{
			//printf("compare: %c(%d) %c(%d)\n", source[i], find[k], i, k);
			if (source[i] != find[k])
			{
				if (find[k] == '*')
				{
					return 0;
				}
				else
				{
					return 1;
				}
			}
			k--;
		}
		return 0;
	}

	if (begin == 0 && end == 1)
	{
		for (int i = 0; i < strlen(source); i++)
		{
			if (source[i] != find[i])
			{
				if (find[i] == '*')
				{
					return 0;
				}
				else
				{
					return 1;
				}
			}
		}
		return 0;
	}

	//if begin & end == 1 (any)
	for (int i = 0; i < strlen(source); i++)
	{
		int found = 1;
		for (int j = 0; j < strlen(find); j++)
		{
			//printf("compare: %c(%d) %c(%d)\n", source[i], i,find[j], j);
			if (i + strlen(find) - 2 > strlen(source))
			{
				return 1;//not found
			}
			if (find[j] != '*')
			{
				if (find[j] != source[i + j])
				{
					found = 0;
					break;
				}
			}
		}
		if (found)
		{
			return 0;
		}
	}
	return 1;
}

void search()
{
	printf("\nSearch use: \n");
	printf("  *word - search through DB all records that ends with 'word'\n");
	printf("  word* - search through DB all records that begins with 'word'\n");
	printf("  *word* - search through DB all records with 'word' in\n\n");
	char criteria[100];
	do
	{
		printf("Search: ");
		scanf("%s", &criteria);
		fflush(stdin);
	} while (strlen(criteria) < 2);

	FILE *f = fopen(FILENAME, "r");
	if (f == NULL)
	{
		return;
	}
	printf("\n ** Searching... %s **\n\n", criteria);
	int found = 0;
	while (1)
	{
		s_phone_user user;
		fscanf(f, "%s %s %s", &user.nom, &user.prenom, &user.tel);
		if (feof(f))
		{
			break;
		}

		found = 0;
		if (str_compare(user.nom, criteria) == 0)
		{	
			found = 1;
		}
		if (str_compare(user.prenom, criteria) == 0)
		{
			found = 1;
		}
		if (str_compare(user.tel, criteria) == 0)
		{
			found = 1;
		}

		if (found)
		{
			printf("%s %s - %s\n", user.nom, user.prenom, user.tel);
		}
	}
	fclose(f);
}
