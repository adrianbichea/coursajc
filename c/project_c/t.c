#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MIN_CODE 32
#define MAX_CODE 125

char crypt_char(char s, char d, int mode)
{
	int roll = MAX_CODE - MIN_CODE;//93
	int c;
	c = s + (d * mode) - MIN_CODE;
	while (c < 0 || c > roll - 1)
	{
		c += roll * (c < 0 ? 1 : -1);
	}
	return (char)(c + MIN_CODE);
}

char *crypt(char *source, char *code, int mode)
{
	char *result = malloc((sizeof(char) * strlen(source)) + 1);

	int j = 0;
	for (int i = 0; i < strlen(source); i++)
	{
		char c;
		int l = crypt_char(source[i], code[j], mode);
		int s = source[i];
		int cd = code[j];

		j++;
		if (j >= strlen(code))
		{
			j = 0;
		}

		result[i] = l;
	}
	return result;
}

int main()
{
	char *source = "John Doe 555-787-888 Jane Doe 555-787-889";

	char *code = "1bcd";

	printf("\nCrypting: '%s' with perroque: '%s'\n", source, code);

	char *result_crypt = crypt(source, code, -1);
	printf("Crypt: '%s'\n", result_crypt);

	char *result_decrypt = crypt(result_crypt, code, 1);
	printf("Decrypt: '%s'\n", result_decrypt);
}
