#include <stdio.h>
#include "annuaire.h"

int main()
{
	char r = '0';
	while (r != 'q')
	{
		printf("1. Add\n");
		printf("2. View\n");
		printf("3.Search\n");
		printf("q.Quit\n");
		printf("Option:");

		scanf(" %c", &r);
		fflush(stdin);

		switch(r)
		{
			case '1':
				add_new();
				break;
			case '2':
				view();
				break;
			case '3':
				search();
				break;
		}
	}
}
