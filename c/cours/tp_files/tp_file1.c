#include <stdio.h>
#include <stdlib.h>

#define FILE_NAME "INFORM.TXT"

void show_menu();
void add_user();
void show_all_users();

int main()
{
	char r = '0';
	system("clear");
	while (r != 'q')
	{
		show_menu();
		scanf(" %c", &r);
		system("clear");
		switch(r)
		{
			case '1':
				add_user();
				break;

			case '2':
				show_all_users();
				break;

			case 'q':
				printf("Quitting...\n");
				break;
			default:
				printf("Option not implemented yet..\n");
				break;
		}
	}
}

void show_menu()
{	
	printf("---- MENU ----\n");
	printf(" 1. New user\n");
	printf(" 2. View all users\n");
	printf(" q. Quit\n\n");
	printf("Option: ");
}

void add_user()
{
	int mat;
	char nom[256];
	char prenom[256];
	printf("\nNumero matricule: ");
	scanf("%d", &mat);
	printf("Nom: ");
	scanf("%s", &nom);
	printf("Prenom: ");
	scanf("%s", &prenom);
	FILE *f = fopen(FILE_NAME, "a+");
	fprintf(f, "%d %s %s ", mat, nom, prenom);
	fclose(f);
	printf("\n  User added sucessfully...\n\n");
}

void show_all_users()
{
	printf("\n  --- ALL USERS ---\n\n");
	FILE *f = fopen(FILE_NAME, "r");
	while (1)
	{
		int mat;
		char nom[256];
		char prenom[256];
		fscanf(f, "%d %s %s", &mat, &nom, &prenom);
		if (feof(f))
		{
			break;
		}
		printf("%4d [%-20.20s] [%-20.20s]\n", mat, nom, prenom);
	}
	fclose(f);
	printf("\n  ------------------\n\n");
}
