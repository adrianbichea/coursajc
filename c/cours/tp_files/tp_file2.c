#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FILE_NAME "INFORM.TXT"
#define NEW_FILE_NAME "INFBIS.TXT"

char *prepare_command()
{
	char *cmd;
	char *c1 = "cp ";
	char *c2 = FILE_NAME;
	char *c3 = " ";
	char *c4 = NEW_FILE_NAME;
	int fullsize = sizeof(c1) + 1 + sizeof(c2) + 1 + 
		sizeof(c3) + 1 + sizeof(c4) + 1;

	cmd = malloc(sizeof(char) * fullsize);

	strcpy(cmd, c1);
	strcat(cmd, c2);
	strcat(cmd, c3);
	strcat(cmd, c4);
	return cmd;
}

int main()
{
	char add[256];
	printf("\nAjout: ");
	fgets(add, 256, stdin);

	char *cmd = prepare_command();

	system(cmd);

	FILE *f = fopen(NEW_FILE_NAME, "a+");
	fprintf(f, "%s", add);
	fclose(f);
}
