#include <stdio.h>
#include <stdlib.h>

#define FILE_NAME "INFORM.TXT"
#define NEW_FILE_NAME "INFBIS.TXT"

int cp(char*, char*);
void append_file(char*, char*);
void show_error_message(int);

int main()
{
	printf("\nAjout:");
	char add[256];
	fgets(add, 256, stdin);
	
	int result = cp(FILE_NAME, NEW_FILE_NAME);
	if (!result)
	{
		show_error_message(result);
		exit(-1);
	}
	
	append_file(NEW_FILE_NAME, add);
}

int cp(char *source_file, char *destination_file)
{
	FILE *fs = fopen(source_file, "r");
	if (fs == NULL)
	{
		return -1;
	}
	FILE *fd = fopen(destination_file, "w");
	if (fd == NULL)
	{
		return -2;
	}
	
	char c;
	while ((c = fgetc(fs)) != EOF)
	{
		fputc(c, fd);
	}
	fclose(fs);
	fclose(fd);
	return 1;
}

void append_file(char *filename, char *text)
{
	FILE *f = fopen(filename, "a+");
	fprintf(f, "%s", text);
	fclose(f);
}

void show_error_message(int error)
{
	printf("\nError occured!\n");
	switch (error)
	{
		case -1:
			printf("\nSource file cannot be opened!\n");
			break;
		case -2:
			printf("\nDestination file cannot be created!\n");
			break;
		default:
			printf("\nUnknown error!\n");
	}
}
