#include <stdio.h>

int calc_surface(int l, int h)
{
	return l * h;
}

int calc_perimetre(int l, int h)
{
	return (2 * l) + (2 * h);
}

void show_calc(char* str, int l, int h)
{
	printf("Ce %s a pour perimetre %d\n", str, calc_perimetre(l, h));
	printf("Ce %s a pour surface %d\n\n", str, calc_surface(l, h));
}

void option_carre()
{
	int cote = 0;
	printf("\n\nQuelle est la taille d'un cote du carre? ");
	scanf("%d", &cote);
	show_calc("carre", cote, cote);
}

void option_rectangle()
{
	int h = 0;
	int l = 0;
	printf("\n\nQuelle este la hauteur et largeur du rectangle? ");
	scanf("%d %d", &h, &l);
	show_calc("rectangle", l, h);
}

void show_menu()
{
	printf("\n*** MENU ***\n");
	printf("1. Calcul carre\n");
	printf("2. Calcul rectangle\n");
	printf("q. Quit\n");
	printf("Option (1, 2 ou q + [ENTER]): ");
}

int main()
{
	char r = '0';
	while(r != 'q')
	{
		show_menu();
		scanf(" %c", &r);

		if (r == '1')
		{
			option_carre();
		}
		else if (r == '2')
		{
			option_rectangle();
		}
		else if (r != 'q')
		{
			printf("\nHeeey!! Seulement 1 2 ou q comme options...\n\n");
		}
	}

	printf("\n\nBye!\n");
	return 0;
}
