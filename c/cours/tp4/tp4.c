#include <stdio.h>

#define PI 3.14159265

float calc_circonference(float r)
{
	return 2.f * PI * r;
}

float calc_diametre(float r)
{
	return 2.f * r;
}

float calc_aire(float r)
{
	return PI * r * r;
}

float calc_volume(float r, float h)
{
	return PI * r * r * h;
}

float calc_vol_cone(float r, float h)
{
	return PI * r * r * h / 3.f;
}

int main()
{
	float rayon = 0.f;
	//Cercle
	printf("Quelle est le rayon du cercle? ");
	scanf("%f", &rayon);
	printf("\nCe cercle a pour diametre %.4f\n", calc_diametre(rayon));
	printf("Ce cercle a pour circonference %.4f\n", calc_circonference(rayon));
	printf("Ce cercle a pour aire %.4f\n\n", calc_aire(rayon));

	//Cylindre
	float hauteur = 0.f;
	printf("Quelle est la hauteur du cylindre? ");
	scanf("%f", &hauteur);
	printf("\nCe cylindre a pour volume %.4f\n", calc_volume(rayon, hauteur));
	//Cone
	printf("\nCe cone a pour volume %.4f\n", calc_vol_cone(rayon, hauteur));
}
