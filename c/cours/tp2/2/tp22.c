#include <stdio.h>

int main()
{
	int age = 23;
	float taille = 1.72f;
	char note = 'B';

	printf("L'utilisateur a %d ans et mesure %.2fm\n", age, taille);
	printf("Il a une note de %c.\n", note);

	return 0;
}
