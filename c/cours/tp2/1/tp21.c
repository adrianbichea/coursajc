#include <stdio.h>
#include <stdlib.h>

int main()
{
	int n_int= 32300;
	long n_long = 123456789;
	short n_short = 2;
	float n_float = 12.3;
	double n_double = 12.42;
	char c = 'a';
	
	printf("Mon int = %d et fait %d octets\n", n_int, sizeof(n_int));
	printf("Mon long = %d et fait %d octets\n", n_long, sizeof(n_long));
	printf("Mon short = %d et fait %d octets\n", n_short, sizeof(n_short));
	printf("Mon float = %f et fait %d octets\n", n_float, sizeof(n_float));
	printf("Mon double = %f et fait %d octets\n", n_double, sizeof(n_double));
	printf("Mon char = '%c' et fait %d octet\n", c, sizeof(c));

	return 0;
}

