#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX_RETRY 10

int rand100()
{
	time_t t;
	srand((unsigned) time(&t));
	return rand() % 100 + 1;
}

int main()
{
	//Generate the random number
	int random_number = rand100();
	int prix;

	printf("-\n----------------------\n");
	printf("Bienvenu au juste prix!\n");
	printf("Vous avez %d essais pour trouver le prix correct!\n", MAX_RETRY);
	printf("Prix entre 1 - 100\n\n");
	for (int i = 0; i < MAX_RETRY; i++)
	{
		printf("%d>", MAX_RETRY - i);
		scanf("%d", &prix);
		if (prix == random_number)
		{
			printf("\n  Bravo!\n", prix);
			break;
		}
		else if (prix > random_number)
		{
			printf("\n  C'est moin!\n");
		}
		else
		{
			printf("\n  C'est plus!\n");
		}
	}

	printf("  Le prix est: %d\n\n", random_number);

	return 0;
}

