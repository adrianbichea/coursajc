#include <stdio.h>

#define TAB_SIZE 10

int add_elements(int *t, int size)
{
	int sum = 0;
	for(int i = 0; i<size; i++)
	{
		sum += t[i];
	}
	return sum;
}

int min_elements(int *t, int size)
{
	int min = t[0];
	for (int i = 1; i < size; i++)
	{
		if (min > t[i])
		{
			min = t[i];
		}
	}
	return min;
}

int max_elements(int *t, int size)
{
	int max = t[0];
	for (int i = 0; i < size; i++)
	{
		if (max < t[i])
		{
			max = t[i];
		}
	}
	return max;
}

int value_exist(int *t, int size, int val)
{
	for (int i = 0; i < size; i++)
	{
		if (t[i] == val)
		{
			return 1;
		}
	}

	return 0;
}

int main()
{
	int tab[TAB_SIZE];

	printf("Please, enter %d values\n", TAB_SIZE);

	for (int i = 0; i < TAB_SIZE; i++)
	{
		printf("  value[%d]: ", i + 1);
		scanf("%d", &tab[i]);
	}

	printf("\n  Somme: %d\n", add_elements(tab, TAB_SIZE));
	printf("  Min: %d\n", min_elements(tab, TAB_SIZE));
	printf("  Max: %d\n", max_elements(tab, TAB_SIZE));

	int find;
	printf("\nSearch number: ");
	scanf("%d", &find);
	char *res_txt = value_exist(tab, TAB_SIZE, find) ? "found" : "not found";

	printf("  Element %d -> %s\n", find, res_txt);
}
