#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define HP 10

int check_word(char*, char*, char);
char* get_random_word();
void draw_pendu(int);

int main()
{
	//preparing
	srand(time(NULL));
	int hp = HP;
	char *hidden_word = get_random_word();
	char *user_word = malloc(sizeof(char) * strlen(hidden_word) + 1);
	for (int i = 0; i < strlen(hidden_word); i++)
	{
		user_word[i] = '_';
	}

	system("clear");
	do
	{
		printf(" *** PENDU GAME ***\n");
		printf("  Mot: %s\n\n", user_word);

		draw_pendu(hp);

		printf(">");

		char c = getchar();
		while (getchar() != '\n'){}//to flush all the remaining chars
		system("clear");
		int found = check_word(hidden_word, user_word, c);
		if (strcmp(user_word, hidden_word) != 0)
		{
			if (found)
			{
				printf("Oui la lettre '%c' est bien presente dans le mot ", c);
			}
			else
			{
				printf("Non la lettre '%c' n'est pas presente dans le mot ", c);
				hp--;
			}
			printf("\"%s\"\n", user_word);
		}
		else
		{
			draw_pendu(hp);
			printf("Bravos! Vous avez trouve le mot \"%s\"\n", user_word);
			printf("Il vous reste %d vies\n", hp);
			break;
		}
	} while (hp > 0);
	if (hp == 0)
	{
		draw_pendu(0);	
		printf("\n  Le mot: %s\n", hidden_word);
	}
}

int check_word(char *hidden, char *user_word, char c)
{
	int r = 0;
	for (int i = 0; i < strlen(hidden); i++)
	{
		if (hidden[i] == c)
		{
			user_word[i] = c;
			r = 1;
		}
	}
	return r;
}

char *get_random_word()
{
	FILE *f = fopen("word_list.txt", "r");
	int word_count;
	fscanf(f, "%d", &word_count);
	int n = (rand() % word_count) + 1;
	int i = 0;
	while (i <= n)
	{
		i++;
		char secret_word[200];
		fscanf(f, "%s", secret_word);
		if (i == n)
		{
			char *ret_string = malloc(sizeof(char) * strlen(secret_word) + 1);
			strcpy(ret_string, secret_word);
			return ret_string;
		}
	}
	return "ERROR";
}

void draw_pendu(int hp)
{
	FILE *f = fopen("graphics.txt", "r");
	int n = HP;
	printf("********\n");
	while (1)
	{
		char s[10];
		fscanf(f, "%s", &s);
		if (feof(f))
		{
			break;
		}
		if (strcmp(s, "BEGIN") == 0)
		{
			while (strcmp(s, "END"))
			{
				fscanf(f, "%s", &s);
				if (n == hp && strcmp(s, "END") != 0)
				{
					printf("*%-6.6s*\n", s);
				}
			}
			n--;
		}
	}
	printf("********\n");
	if (hp == 0)
	{
		printf("\n   ----- PENDU! ---\n");
	}
}
