#include <stdio.h>
#include <stdlib.h>

#define PLAYER1 0
#define PLAYER2 1
#define SIZE 3

char get_player(int p);
void newgame(int*, int);
void show_table(int*,int);
int play(int, int, int, int*, int);
int game_status(int*, int);

int *winners;
int lines = 1;

int main()
{
	int *grid;

	int current_player = 0;

	grid = malloc(sizeof(int) * SIZE * SIZE);
	newgame(grid, SIZE);

	while (game_status)
	{
		show_table(grid, SIZE);
		printf("Player %c, play (x, y): ", get_player(current_player));
		int x, y;
		scanf("%d %d", &x, &y);

		int cp = play(current_player, x, y, grid, SIZE);
		if (cp == current_player)
		{
			printf("\n   Already ocupied!\n\n");
		}
		
		current_player = cp;

		int gs = game_status(grid, SIZE);
		if (gs != 2)
		{
			show_table(grid, SIZE);
			switch (gs)
			{
				case 0:
					printf("Player X WON!\n");
					break;
				case 1:
					printf("Player 0 WON!\n");
					break;
				case 3:
					printf("DRAW\n");
					break;
			}
			break;
		}
	}
}

void newgame(int *grid, int size)
{
	FILE *f = fopen("check.txt", "r");
	int v1, v2, v3;
	int first_time = 1;
	int idx = 0;
	int line = 1;
	while (1)
	{
		fscanf(f, "%d %d %d", &v1, &v2, &v3);
		if (feof(f))
		{
			break;
		}
		if (first_time)
		{
			first_time = 0;
			winners = malloc(sizeof(int) * size);
			line++;
		}
		else
		{
			winners = realloc(winners, sizeof(int) * size * line);
			line++;
		}
		winners[idx] = v1;
		winners[idx + 1] = v2;
		winners[idx + 2] = v3;
		idx += 3;

	}
	lines = line - 1;

	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			grid[(i * size) + j] = -1;
		}
	}
}

void show_table(int *grid, int size)
{
	printf("  ");
	for (int i = 0; i < size; i++)
	{
		printf("|%d", i + 1);
	}
	printf("|\n");

	for (int i = 0; i < size; i++)
	{
		printf("%d |", i + 1);
		for (int j = 0; j < size; j++)
		{
			printf("%c|", get_player(grid[(i * size) + j]));
		}
		printf("\n");
	}
}

char get_player(int p)
{
	return p == 0 ? 'X' : p == 1 ? '0' : ' ';
}

int play(int player, int _x, int _y, int *grid, int size)
{
	int x = _x - 1;
	int y = _y - 1;
	if (x < 0 || x >= size || y < 0 || y >= size)
	{
		return player;//play again
	}
	if (grid[(y * size) + x] != -1)
	{
		return player;//play again
	}

	grid[(y * size) + x] = player;

	return 1 - player;
}

int game_status(int *grid, int size)
{
	for (int i = 0; i < lines; i++)
	{
		if (grid[winners[(i * size)]] != -1)
		{
			int a = grid[winners[(i * size) + 0]];
			int b = grid[winners[(i * size) + 1]];
			int c = grid[winners[(i * size) + 2]];
			if (a == b && a == c)
			{
				return a;
			}
		}
	}
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			if (grid[(i * 3) + j] == -1)
			{
				return 2;//not finished
			}
		}
	}
	return 3;//draw
}

