#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define FACES 6
#define THROWS 3

int throw(int faces);
int multi_throws(int faces, int times);

int main()
{
	srand(time(NULL));

	int res = multi_throws(FACES, THROWS);

	printf("Lance %d des de %d: %d\n", THROWS, FACES, res);

	return 0;
}

int throw(int faces)
{
	return (rand() % faces) + 1;
}

int multi_throws(int faces, int times)
{
	int sum = 0;
	for (int i = 0; i < times; i++)
	{
		sum += throw(faces);
	}
	return sum;
}
