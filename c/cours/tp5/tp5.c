#include <stdio.h>

int main()
{
	float n1, n2;
	char op;

	printf("-------------------------------------------------------------------\n");
	printf("The mighty calculator\n");
	printf("Indiquez l'operation mathematique que je doit resoudre (sans espace)\n");
	printf("  - sous la forme [nombre1][operateur][nombre2] par example (2+3)\n");
	printf("  - operateurs possibles [+, -, *, /]\n");
	printf(">");
	
	scanf("%f%c%f", &n1, &op, &n2);

	switch(op)
	{
		case '+':
			printf("Resultat: %f", n1 + n2);
		break;

		case '-':
			printf("Resultat: %f", n1 - n2);
		break;

		case '*':
			printf("Resultat: %f", n1 * n2);
		break;

		case '/':
			if (n2 != 0)
			{
				printf("Resultat: %f", n1 / n2);
			}
			else
			{
				printf("Resultat: infinity");
			}
		break;

		default:
			printf("Operateur invalide! Operateurs [+, -, *, /]");
		break;
	}

	printf("\n\n");

	return 0;
}
