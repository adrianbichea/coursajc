#include <stdio.h>

typedef enum Status
{
	OK = 0,
	NOT_OK
}e_status;

typedef struct Car
{
	char brand[256];
	char model[256];
	double fuel;
	double speed;
	e_status status;
}s_car;

void create_car(s_car*);
void view_car(s_car*);

int main()
{
	s_car car;
	create_car(&car);

	view_car(&car);

	return 0;
}

void create_car(s_car *car)
{
	printf("Car creation!\n");
	printf("  Enter car brand: ");
	scanf("%s", &car->brand);
	printf("  Enter car model: ");
	scanf("%s", &car->model);
	printf("  Enter top speed: ");
	scanf("%lf", &car->speed);
	car->fuel = 80;
	car->status = OK;
}

void view_car(s_car *car)
{
	printf("\n\n-------\nCar infos\n");
	printf("  Brand: %s\n", car->brand);
	printf("  Model: %s\n", car->model);
	printf("  Top speed: %.2f km/h\n", car->speed);
	printf("  Fuel level: %.2f %\n", car->fuel);
	printf("  Status: %s\n\n", car->status == OK ? "OK" : "NOT OK");
}
