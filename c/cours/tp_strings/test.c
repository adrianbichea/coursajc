#include <stdio.h>
#include <string.h>

int main()
{
	char t[20] = "Test";

	printf("Size of '%s' is %d chars\n", t, strlen(t));

	strcpy(t, "another test");

	printf("Size of '%s' is %d chars\n", t, strlen(t));

	char m1[] = "Jason";
	int test = strcmp(m1, t);
	if (test == 0)
	{
		printf("Same text!\n");
	}
	else if (test < 0)
	{
		printf("%s < %s\n", m1, t);
	}
	else
	{
		printf("%s > %s\n", m1, t);
	}

	sprintf(t, "HH", 2);
	printf("%s\n", t);

	strcpy(t, "aosdiapsoid");
	printf("%s\n", t);

	return 0;
}
