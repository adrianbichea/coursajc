#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define COLORS "RVBJO"
#define MAX_CODE_LEN 4
#define TRIES 10

char code[MAX_CODE_LEN + 1];

int get_random(int min, int max)
{
	return (rand() % (max + 1 - min)) + min;
}

void generate_random_code();
char show_menu();
int same_colors(char *code, char *try_code, int len);
int other_colors(char *code, char *try_code, int len);

int main()
{
	srand(time(NULL));

	char r = 'o';
	while (r == 'o' || r == 'O')
	{
		system("clear");
		printf("\n-- MASTERMIND --\n");

		char code_try[5];
		int tries = TRIES;
		generate_random_code();
		//strcpy(code, "OJRR");
		printf("Donnez un code de 4 couleurs differentes et sans espace.\n");
		printf("Couleurs {R, V, B, J, O}\n");

		while (tries > 0)
		{
			printf(">");
			scanf("%s", &code_try);
			fflush(stdin);

			if (strlen(code_try) != 4)
			{
				printf("SVP! Code de 4 couleurs!\n");
				continue;
			}

			if (strcmp(code_try, "show") == 0)
			{
				printf("\n Cheater!! %s \n\n", code);
			}

			tries--;
			printf("Tentative: %d/%d\n", TRIES - tries, TRIES);
			printf("Couleurs mal placees: %d\n", 
					other_colors(code, code_try, MAX_CODE_LEN));
			int same = same_colors(code, code_try, MAX_CODE_LEN);
			printf("Couleurs bien placees: %d\n", same);

			if (same == MAX_CODE_LEN)
			{
				tries = 10;
				break;
			}
		}

		if (tries != 0)
		{
			printf("  YOU WON!\n");
		}
		else
		{
			printf("  YOU LOSE!\n");
		}

		printf("\n * CODE * -> '%s'\n\n", code);

		r = show_menu();
	}
}

char show_menu()
{
		printf("Rejouer? (o/n)");
		char r;
		scanf(" %c", &r);
		fflush(stdin);
		return r;
}

int same_colors(char *code, char *try_code, int len)
{
	int same = 0;
	for (int i = 0; i < len; i++)
	{
		if (code[i] == try_code[i])
		{
			same++;
		}
	}
	return same;
}

int other_colors(char *code, char *try_code, int len)
{
	int other = 0;
	for (int i = 0; i < len; i++)
	{
		if (code[i] != try_code[i])
		{
			//search this char into the rest of the string
			for (int j = 0; j < len; j++)
			{
				//need to exclude the same color
				if (code[j] != try_code[j])
				{
					if (try_code[i] == code[j])
					{
						other++;
		//printf("%d, %d, %c, %c\n", i, j, code[j], try_code[j]);
						break;
					}
				}
			}
		}
	}
	return other;
}

//Generate unique colors
void generate_random_code()
{
	int i = 0;
	char *colors;
	strcpy(colors, COLORS);
	while (i < 4)
	{
		int is_ok = 1;
		int cod = colors[get_random(1, strlen(colors) - 1)];

		for (int j = 0; j < i; j++)
		{
			if (code[j] == cod)
			{
				is_ok = 0;
			}
		}

		if (is_ok)
		{
			code[i++] = cod;
		}
		
	}
}
