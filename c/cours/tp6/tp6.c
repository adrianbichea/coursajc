#include <stdio.h>
#include <string.h>

#define MAX_RETRY 3
#define MAX_DIGITS 4
#define print_red(str) printf("\033[0;31m%s\033[0m\n", (str))
#define print_green(str) printf("\033[0;32m%s\033[0m\n", (str))


int check_pin(char* input_pin, int retry)
{
	char pin[MAX_DIGITS];
	strcpy(pin, input_pin);
	if (strcmp(pin, "0000"))
	{
		printf("   Bad pin code!\n   Remaining retries: %d\n", retry);
		if (retry <= 0)
		{
			print_red("\n\n   --- PHONE LOCKED! ---\n");
			return -1;
		}
	}
	else
	{
		print_green("\n\n    *** PHONE UNLOCKED! ***\n");
		return 1;
	}
	return 0;
}

int main()
{
	for (int retry = MAX_RETRY - 1; retry >= 0; retry--)
	{
		char* input_pin;
		printf("\n\n------------");
		printf("\nPin code (%d digits): ", MAX_DIGITS);
		scanf("%s", input_pin);
	
		if (strlen(input_pin) != MAX_DIGITS)
		{
			printf("   Pin code must have %d digits!\n", MAX_DIGITS);
			strcpy(input_pin, "err0");
		}
		if (check_pin(input_pin, retry) != 0)
		{
			break;
		}
	}
	printf("\033[0m");
}
