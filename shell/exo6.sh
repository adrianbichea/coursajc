#!/bin/bash

for user in $(cat /etc/passwd | cut -d: -f1)
do
       	echo "----------------------"
        echo "User: $user"

       	cmdLast="last $user"
        eval $cmdLast
done

echo ""
echo "----------------------"
echo "Users connected: "
who | cut -d' ' -f1
echo "----------------------"
