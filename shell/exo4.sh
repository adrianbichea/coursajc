#!/bin/bash

if [ $# != 1 ]
then
        echo "Usage: $0 [username]"
else
        ps -u $1
fi
