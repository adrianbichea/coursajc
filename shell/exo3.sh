#!/bin/bash

while true
do
        echo "------------------------"
        echo -n "New directory name: "
        read newDir

        echo "*****************************"
        if [ -d "$newDir" ]
        then
                echo "Directory already exist!"
                echo "Choose another directory name"
        else
                echo "Directory created!"
                eval mkdir $newDir
                break
        fi

        echo "*****************************"
done

echo "Exiting program..."