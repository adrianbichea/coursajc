#!/bin/bash

printUser () {
        echo -n "Username : "
	read user
        if grep -w "^$user" /etc/passwd > /dev/null
        then
                echo "$user - found"
        else
                echo "$user - not found"
        fi
}

checkUID () {
        echo -n "Username : "
        read user
        if grep -w "^$user" /etc/passwd > /dev/null
        then
                echo -n "UID ($user): "
                eval grep -w "^$user" /etc/passwd | cut -d: -f3
        else
                echo "$user - not found"
        fi
}

r="0"
while [ $r != 3 ]
do
        echo "-----------------------"
        echo "1. Check if user exists"
        echo "2. Check user UID"
        echo "3. Quit"
        echo -n "Option: "
        read r
	clear

        case $r in
                1) printUser
                ;;
                2) checkUID
                ;;
		3) echo "Quitting ... Goodbye!"
		;;
                *) echo "Naah! You can choose 1, 2 or 3 to do an action"
                ;;
        esac
done
