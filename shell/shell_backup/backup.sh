#!/bin/bash

if [ $# != 1 ]; then
	echo "Use: $0 <backup directory>"
	exit
fi

NOW=$(date +"%d-%m-%Y")


if [[ "$1" == */ ]]; then
	backup_dir="$1${NOW}"
else
	backup_dir="$1/${NOW}"
fi

mkdir -p $backup_dir

tar -czvf "$backup_dir/backup.tar.gzip" /var/www/html/*
