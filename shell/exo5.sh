#!/bin/bash

if [ $# == 0 ]
then
        echo "Usage: $0 [username1 username2 ...]"
else
        for arg in "$@"
        do
                echo "------------------------"
                echo "Infos for user: $arg, are ..."

                connDir="grep -w \"^$arg\" /etc/passwd | cut -d: -f6"
                echo -n "Connection directory: "
                eval $connDir

                shellPrg="grep -w \"^$arg\" /etc/passwd | cut -d: -f7"
                echo -n "Shell: "
                eval $shellPrg
        done
fi
