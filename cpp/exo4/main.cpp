#include "eleve.h"

int main()
{
    Eleve e;
    e.init(2, "Haha!");

    e.afficher();

    e.setNote(122);
    e.afficher();

    e.setObs("123456789012345678901234567890overflow");
    e.afficher();

    e.init(20, "Wow!");

    e.afficher();

    cout << "(Main) Note " << e.getNote() << " obs: " << e.getObs() << endl;

    return 0;
}
