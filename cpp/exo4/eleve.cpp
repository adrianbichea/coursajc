#include <string.h>

#include "eleve.h"

void Eleve::init(float note, const char* obs)
{
    setNote(note);
    setObs(obs);
}

void Eleve::afficher()
{
    cout << "(Class Eleve) Note " << note << " obs: " << obs << endl;
}

void Eleve::setNote(float _note)
{
    if (_note >= 0.0f && _note <= 20.0f)
    {
        note = _note;
    }
    else
    {
        note = -1.0f;
    }
}

float Eleve::getNote()
{
    return note;
}

void Eleve::setObs(const char* _obs)
{
    if (strlen(_obs) > MAX_OBS_LENGHT)
    {
        strcpy(obs, ERROR_OBS_MESSAGE);
        //strncpy(obs, _obs, MAX_OBS_LENGHT);
    }
    else
    {
        strcpy(obs, _obs);
    }
}

const char* Eleve::getObs()
{
    return obs;
}
