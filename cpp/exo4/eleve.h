#ifndef ELEVE_H_INCLUDED
#define ELEVE_H_INCLUDED

#include <iostream>

using namespace std;

#define MAX_OBS_LENGHT 30
#define ERROR_OBS_MESSAGE "Obs non saisie"

class Eleve
{
private:
    float note;
    char obs[MAX_OBS_LENGHT + 1];

public:
    void init(float, const char*);
    void afficher();

    //get-set note
    void setNote(float note);
    float getNote();

    //get-set obs
    void setObs(const char*);
    const char* getObs();

};

#endif // ELEVE_H_INCLUDED
