#ifndef _CODED_H_
#define _CODED_H_

void coded_open(char *, char *);
void coded_close();

void coded_set_perroquet(char *);
char *coded_next_line();

int is_eof();

void coded_add_line(char *);

#endif
