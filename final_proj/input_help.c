#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "input_help.h"

char* get_input(char *message, int size)
{
	printf("%s", message);
	char s[size];
	
	fgets(s, size, stdin);
	if (strlen(s) > 0 && (s[strlen(s) - 1] == '\n'))
	{
		s[strlen(s) - 1] = '\0';
	}
	fflush(stdin);

	char *input = malloc((sizeof(char) * strlen(s)) + 1);
	strcpy(input, s);
	return input;
}


