#include <string.h>
#include <stdlib.h>

#include "lang.h"

char *language;
char *lang_code[100];
char *lang_text[100];

void load_language()
{
	for (int i = 0; i < 100; i++)
	{
		//printf("feeing: %d\n", i); 
		if (lang_code[i] != NULL)
		{	
			free(lang_code[i]);
			lang_code[i] = NULL;
		}
		if (lang_text[i] != NULL)
		{
			free(lang_text[i]);
			lang_text[i] = NULL;
		}
	}

	//printf("0\n");
	char *filename = NULL;
	//printf("1\n");
	char *fn = "language_";
	//printf("2\n");
	filename = malloc(strlen(fn) + strlen(language) + 2); 
	//printf("3\n");
	strcpy(filename, fn);
	//printf("4\n");
	//printf(" %s", filename);
	//printf("5\n");
	strcat(filename, language);
	//printf("6\n");
	//printf(" %s", filename);

	FILE *f = fopen(filename, "r");

	if (f == NULL)
	{
		printf("LANGUAGE FILE %s NOT FOUND!\n", filename);
		free(filename);
		return;
	}

	int _feof = 0;
	char cod[10];
	char str[200];
	unsigned char c;
	int idx = 0;
	int cnt = 0;
	int i_lang = 0;
	while ((c = fgetc(f)) != EOF)
	{
		if (c != '\n')
		{
			if (c != '|')
			{
				if (cnt == 0)
				{
					cod[idx++] = c;
					cod[idx] = '\0';
				}
				else
				{
					str[idx++] = c;
					str[idx] = '\0';
				}
			}
			else
			{
				idx = 0;
				cnt++;
			}
		}
		else
		{
			idx = 0;
			cnt = 0;
			if (strlen(cod) == 0 || strlen(str) == 0)
			{
				break;
			}
			lang_code[i_lang] = malloc(sizeof(cod) + 1);
			strcpy(lang_code[i_lang], cod);
			lang_text[i_lang] = malloc(sizeof(str) + 1);
			strcpy(lang_text[i_lang], str);
			i_lang++;
			cod[0] = '\0';
			str[0] = '\0';
		}
	}
	fclose(f);
	//printf("%s ", filename);
	free(filename);
	//printf("end loading file!\n");
}

//change_lang("fr");, en, es
void change_lang(char *lang)
{
	if (language != NULL)
	{
		free(language);
	}
	language = malloc(strlen(lang) + 1);
	strcpy(language, lang);
	load_language();
}

char *get_lang(char *string)
{
	for (int i = 0; i < 100; i++)
	{
		if (lang_code[i] == NULL)
		{
			break;
		}
		if (strcmp(lang_code[i], string) == 0)
		{
			return lang_text[i];
		}
	}
	return NULL;
}

void destroy_lang()
{
	free(language);
}

